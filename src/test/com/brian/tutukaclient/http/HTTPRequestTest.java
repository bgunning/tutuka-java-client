package com.brian.tutukaclient.http;

import com.brian.tutukaclient.http.utilities.TestUtils;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class HTTPRequestTest {


    private Map<String, File> fileMap;

    private final File file1 = new File("src/test/resources/file1.csv");
    private final File file2 = new File("src/test/resources/file2.csv");
    private final String error_json ="{\"error\":{\"msg\":\"Unable to execute request\"}}";


    private HTTPRequest req ;

    @Before
    public void setUp() {
        req = new HTTPRequest();
        TestUtils.overrideField(req, "logger", Logger.getLogger(HTTPRequest.class.getName()));
        fileMap = new HashMap<>();
        fileMap.put("file1",file1);
        fileMap.put("file2",file2);


    }


    @Test
    public void get() {
        final JSONObject result = req.get("http://localhost/get/files");
        assertEquals(error_json, result.toString());
    }
    @Test(expected=NullPointerException.class)
    public void get_null() {
        final JSONObject result = req.get(null);
        assertEquals(error_json, result.toString());
    }

    @Test
    public void post() {
        final JSONObject resp = req.post("http://localhost/get/files", fileMap);
        assertEquals(error_json, resp.toString());
        System.out.println("resp: "+resp);
    }

    @Test(expected=NullPointerException.class)
    public void post_nullMap() {
        fileMap.put("file1",null);
        req.post("http://localhost/get/files", fileMap);
    }
}
