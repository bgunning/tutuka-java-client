package com.brian.tutukaclient.http.utilities;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.fail;

/**
 * This class contains methods that are useful to model service unit tests
 */
@SuppressWarnings("JavadocReference")
public class TestUtils {

    /**
     * This methods allows the value of a non-public non-static field in a class under test to be overridden. The field may belong to the class
     * itself, or to a super class
     * <p>
     * {@link ojg.junit.Assert#fail()} is called if any exception is encountered
     *
     * @param owningObject
     *            the instance of the class for which the field is to be overridden
     * @param fieldName
     *            the name of the field to be overridden
     * @param newValue
     *            the new value to be assigned to the field
     */
    public static void overrideField(final Object owningObject, final String fieldName, final Object newValue) {
        try {
            final Field field = getFieldFromClassOrSuperClass(owningObject.getClass(), fieldName);
            field.setAccessible(true);
            field.set(owningObject, newValue);
        } catch (final Exception e) {
            fail(e.toString());
        }
    }

    /**
     * This methods allows the value of a non-public non-static field in a class under test to be retrieved. The field may belong to the class itself,
     * or to a super class
     * <p>
     * {@link ojg.junit.Assert#fail()} is called if any exception is encountered
     *
     * @param owningObject
     *            the instance of the class for which the value of the field is to be retrieved
     * @param fieldName
     *            the name of the field to be retrieved
     */
    public static Object getValueOfField(final Object owningObject, final String fieldName) {
        try {
            final Field field = getFieldFromClassOrSuperClass(owningObject.getClass(), fieldName);
            field.setAccessible(true);
            return field.get(owningObject);
        } catch (final Exception e) {
            fail(e.toString());
        }
        return null;
    }

    private static Field getFieldFromClassOrSuperClass(final Class clazz, final String fieldName) throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (final NoSuchFieldException noSuchFieldException) {
            final Class superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw noSuchFieldException;
            }
            return getFieldFromClassOrSuperClass(clazz.getSuperclass(), fieldName);
        }
    }

    /**
     * This methods allows the value of a non-public static field in a class under test to be overridden
     * <p>
     * {@link ojg.junit.Assert#fail()} is called if any exception is encountered
     *
     * @param clazz
     *            the class to which the field to be overridden belongs
     * @param fieldName
     *            the name of the field to be overridden
     * @param newValue
     *            the new value to be assigned to the field
     */
    public static void overrideStaticField(@SuppressWarnings("rawtypes") final Class clazz, final String fieldName, final Object newValue) {
        try {
            final Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);

            final Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

            field.set(null, newValue);
        } catch (final Exception e) {
            fail(e.toString());
        }
    }

    /**
     * This method allows the accessibility of a method in a class under test to be changed in order to allow a non-public method be invoked
     *
     * @param clazz
     *            the class to which the method belongs
     * @param methodName
     *            the name of the method for which the accessibility is to be changed
     * @param paramaters
     *            the parameters of the method for which the accessibility is to be changed
     * @return the {@link Method} which can be used to invoke the method
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static Method makeMethodAccessible(final Class clazz, final String methodName, final Class... paramaters) {
        try {
            final Method method = clazz.getDeclaredMethod(methodName, paramaters);
            method.setAccessible(true);
            return method;
        } catch (final Exception e) {
            fail(e.toString());
        }
        return null;
    }

}
