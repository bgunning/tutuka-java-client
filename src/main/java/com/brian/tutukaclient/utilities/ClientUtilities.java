package com.brian.tutukaclient.utilities;

import org.apache.commons.validator.routines.UrlValidator;

import java.io.File;

public class ClientUtilities {

    private static final UrlValidator urlValidator = new UrlValidator(UrlValidator.ALLOW_LOCAL_URLS);


    public static boolean validateURL(final String url){

        return urlValidator.isValid(url);

    }

    public static boolean validateFile(final File file) {
        return file.isFile();

    }
}
