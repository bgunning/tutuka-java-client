package com.brian.tutukaclient.cliclient;

import com.brian.tutukaclient.http.HTTPRequest;
import com.brian.tutukaclient.utilities.ClientUtilities;
import com.brian.tutukaclient.utilities.StringConstants;
import org.apache.commons.cli.*;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Interpret arguments and flags and take appropriate action
 */
class CliInterpreter {
    private static final Logger logger = Logger.getLogger(CliInterpreter.class.getName());
    private final String[] args;
    private final Options options;
    private String url;
    private File file1;
    private File file2;
    private String action;



    public CliInterpreter(final String[] args) {

        this.args = args;
        options = CliClientUtilities.configureClientOptions();

    }


    /**
     * Instantiate Parser, parse arguments and decide HTTP request to make
     */
    public void initialise() {

        final CommandLineParser parser = new DefaultParser();
        final HTTPRequest request = new HTTPRequest();

        final CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
            initialParseArguments(cmd);
            decideAction(action, url, request, cmd);

        } catch (final ParseException e) {
            logger.log(Level.SEVERE, "Failed to initialise command line properties", e.getMessage());
            CliClientUtilities.help(options);
        }

    }

    /**
     * @param action - the requested HTTP action
     * @param url - the url
     * @param request - the request
     * @param cmd - the CommandLine object
     */
    private void decideAction(final String action, final String url, final HTTPRequest request, final CommandLine cmd) {

        switch (action) {

            case "GET": {
                logger.fine("Execute GET");
                System.out.println(request.get(url));
                break;
            }
            case "POST": {
                logger.fine("Execute POST");
                parseAdditionalArguments(cmd);
                final Map<String, File> fileMap = new HashMap<>();
                fileMap.put("file1", file1);
                fileMap.put("file2", file2);
                System.out.println(request.post(url, fileMap));
                break;
            }
            default:
                logger.severe("UNSUPPORTED HTTP ACTION, GET & POST SUPPORTED");
                System.out.println("{error: UNSUPPORTED HTTP ACTION, GET & POST SUPPORTED}");
                break;


        }

    }

    /**
     * Parse File options if POST selected
     * @param cmd - the CommandLine object
     */
    private void parseAdditionalArguments(final CommandLine cmd) {

        if (cmd.hasOption(StringConstants.F1)) {
            file1 = new File(cmd.getOptionValue(StringConstants.F1));
            final boolean isValid = ClientUtilities.validateFile(file1);
            if (!isValid) {
                logger.severe("File does not exist: " + file1);
                CliClientUtilities.help(options);
            }
        } else {
            logger.severe("Missing mandatory paramter for POST '-f1': ");
            CliClientUtilities.help(options);
        }

        if (cmd.hasOption(StringConstants.F2)) {
            file2 = new File(cmd.getOptionValue(StringConstants.F2));
            final boolean isValid = ClientUtilities.validateFile(file2);
            if (!isValid) {
                logger.severe("File does not exist: " + file2);
                CliClientUtilities.help(options);
            }
        } else {
            logger.severe("Missing mandatory paramter for POST '-f2': ");
            CliClientUtilities.help(options);
        }

    }

    /**
     * Initial parsing of retrieved command line arguments
     * @param cmd  - the CommandLine object
     */
    private void initialParseArguments(final CommandLine cmd) {

        if (cmd.hasOption(StringConstants.U)) {
            url = cmd.getOptionValue(StringConstants.U);
            final boolean isValid = ClientUtilities.validateURL(url);
            if (!isValid) {
                logger.severe("Invalid URL: " + url);
                CliClientUtilities.help(options);
            }
        }

        if (cmd.hasOption(StringConstants.A)) {
            action = cmd.getOptionValue(StringConstants.A);
            logger.fine("HTTP ACTION SELECTED: " + action);

        }


    }


}
