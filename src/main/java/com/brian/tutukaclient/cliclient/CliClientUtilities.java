package com.brian.tutukaclient.cliclient;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

class CliClientUtilities {


    public static Options configureClientOptions() {
        final Options options = new Options();

        final Option actionOption = Option.builder("a")
                .longOpt("action")
                .required(true)
                .desc("Mandatory, HTTP action valid options are 'GET' or 'POST' ")
                .hasArg() // This option has an argument.
                .build();

        final Option file1Option = Option.builder("f1")
                .longOpt("file1")
                .required(false)
                .desc("Mandatory if POST selected, full path to a file")
                .hasArg() // This option has an argument.
                .build();

        final Option file2Option = Option.builder("f2")
                .longOpt("file2")
                .required(false)
                .desc("Mandatory if POST selected, full path to a file")
                .hasArg() // This option has an argument.
                .build();

        final Option urlOption = Option.builder("u")
                .longOpt("url")
                .required(true)
                .desc("Mandatory, URL for the request")
                .hasArg() // This option has an argument.
                .build();


        options.addOption(actionOption);
        options.addOption(file1Option);
        options.addOption(file2Option);
        options.addOption(urlOption);
        options.addOption("h", "help", false, "show help.");
        return options;
    }


    /**
     * Print usage
     * @param options - available options
     */
    public static void help(final Options options) {
        // This prints out some help
        final HelpFormatter formatter = new HelpFormatter();
        final String header="\nWelcome to the Tutuka Java Client\nIt provides a simple interface to the Tutuka File Reconciliation Service\n";
        final String footer = "\n";
//        formatter.printHelp("Main", options);
        formatter.printHelp("java -jar javaclient.jar", header, options, footer, true);
        System.exit(0);
    }
}
