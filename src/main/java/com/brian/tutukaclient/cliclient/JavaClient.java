package com.brian.tutukaclient.cliclient;

/**
 * Java Client to use against Tutuka File Reconciliation Service
 * java -jar client-1.0-SNAPSHOT-jar-with-dependencies.jar -h for usage
 */
public class JavaClient {


    public JavaClient(){

    }

    public static void main(final String[] args) {

        new CliInterpreter(args).initialise();

    }
}
