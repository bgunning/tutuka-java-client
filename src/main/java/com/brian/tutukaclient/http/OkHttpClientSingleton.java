package com.brian.tutukaclient.http;

import okhttp3.OkHttpClient;

class OkHttpClientSingleton {

    private static final OkHttpClient okHttpClient = new OkHttpClient();

    private OkHttpClientSingleton(){}

    public static OkHttpClient getInstance(){
        return okHttpClient;
    }
}
