package com.brian.tutukaclient.http;

import org.json.JSONObject;

import java.io.File;
import java.util.Map;

@SuppressWarnings("unused")
interface IHTTPRequest {

    JSONObject get(String url);
    JSONObject post(String url, Map<String, File> fileMap);
}
