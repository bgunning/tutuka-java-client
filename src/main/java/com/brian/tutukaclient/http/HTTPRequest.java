package com.brian.tutukaclient.http;

import okhttp3.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;

public class HTTPRequest implements IHTTPRequest {

    private final Logger logger = Logger.getLogger(HTTPRequest.class.getName());

    @Override
    public JSONObject get(final String url) {

        final OkHttpClient client = OkHttpClientSingleton.getInstance();

        logger.fine("Building request");
        final Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

            return execute(client, request);

    }


    /**
     * Execute the HTTP request
     *
     * @param client - the HTTP client
     * @param request - the Request
     * @return - JSON response
     */
    private JSONObject execute(final OkHttpClient client, final Request request) {
        String str="";
        try {
            final Response response = client.newCall(request).execute();
            str = Objects.requireNonNull(response.body()).string();
            return new JSONObject(str);
        } catch (final IOException e) {
            logger.severe(e.getMessage());
        }
        catch(final JSONException e){
            //usually a server exception or 404
        }
        return buildErrorResponse("Unable to execute request");
    }


    /**
     * Build the POST request
     * @param url - the URL
     * @param fileMap - map of files to be uploaded
     * @return - JSON response
     */
    @Override
    public JSONObject post(final String url, final Map<String, File> fileMap) {

        final OkHttpClient client = OkHttpClientSingleton.getInstance();
        final MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);

        try {
            for (final Map.Entry entry : fileMap.entrySet()) {
                final String contentType;
                final Path path = Paths.get(((File) entry.getValue()).getAbsolutePath());
                contentType = Files.probeContentType(path);
                final MediaType FILE_MEDIA_TYPE = MediaType.parse(contentType);
                builder.addFormDataPart((String) entry.getKey(), ((File) entry.getValue()).getName(), RequestBody.create(FILE_MEDIA_TYPE, (File) entry.getValue()));

            }
        } catch (final IOException e) {
            logger.severe("Unable to decipher file type");
            return buildErrorResponse(e.getMessage());
        }

        final RequestBody requestBody = builder.build();
        final Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();

        return execute(client, request);

    }

    private JSONObject buildErrorResponse(final String msg) {

        final JSONObject error = new JSONObject();
        error.put("msg", msg);
        final JSONObject enclosingJSON = new JSONObject();
        enclosingJSON.put("error", error);
        return enclosingJSON;

    }
}
