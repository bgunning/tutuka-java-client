# README #
This is a java client for use with this [File Reconciliation Service](https://bitbucket.org/bgunning/tutuka-server/)


### How do I get set up? ###

* git pull
* mvn clean install

Alterntaively download the compiled ```jar``` from [here](https://bitbucket.org/bgunning/tutuka-java-client/downloads)

### Requirements ###

* Java 8


### How do I use it ###

The use case for the File Reconciliation Service is as follows

1. Upload two compliant CSV files for comparison
2. Summary result is returned
3. Run any of the three available algorithms for deeper analysis. This will recommend potential matches for unmatched transactions if they exist and highlight any still unmatched transactions and any duplicate entries.
4. Algorithms can be run as often as is wished.


### Usage ###

```
usage: java -jar javaclient.jar -a <arg> [-f1 <arg>] [-f2 <arg>] [-h] -u
       <arg>

Welcome to the Tutuka Java Client
It provides a simple interface to the Tutuka File Reconciliation Service
 -a,--action <arg>   Mandatory, HTTP action valid options are 'GET' or
                     'POST'
 -f1,--file1 <arg>   Mandatory if POST selected, full path to a file
 -f2,--file2 <arg>   Mandatory if POST selected, full path to a file
 -h,--help           show help.
 -u,--url <arg>      Mandatory, URL for the request

```

**Uploading Files**

```

java -jar target/javaclient.jar -a POST -u http://localhost/tutuka/api/v1/upload/files -f1 ~/Documents/me/tutuka/ClientMarkoffFile20140113.csv  -f2 ~/Documents/me/tutuka/TutukaMarkoffFile20140113.csv

```

***Response***

The ```id``` value in the response is the ```id``` to be used in subsequent executions of matching algorithms

```
{"fileOneTotal":306,"fileTwoTotal":305,"fileTwoUnmatched":16,"fileTwoDuplicates":1,"fileTwoMatching":288,"fileOneMatching":288,"fileOneUnmatched":16,"fileTwoName":"TutukaMarkoffFile20140113.csv","id":118,"fileOneName":"ClientMarkoffFile20140113.csv","fileOneDuplicates":2}

```

**Reconcile Files - Basic Algorithm**

```
java -jar target/javaclient.jar -a GET -u http://localhost/tutuka/api/v1/reconcile/column/{id}
e.g.
java -jar target/javaclient.jar -a GET -u http://localhost/tutuka/api/v1/reconcile/column/7991/

```

**Reconcile Files - Fuzzy Logic Algorithm**

```
java -jar target/javaclient.jar -a GET -u http://localhost/tutuka/api/v1/reconcile/fuzzy/{id}/{fuzzy_ratio}
e.g.
java -jar target/javaclient.jar -a GET -u http://localhost/tutuka/api/v1/reconcile/fuzzy/7991/90

```

**Reconcile Files - TransactionID Algorithm**

```
java -jar target/javaclient.jar -a GET -u http://localhost/tutuka/api/v1/reconcile/transaction/{id}
e.g.
java -jar target/javaclient.jar -a GET -u http://localhost/tutuka/api/v1/reconcile/transaction/7991

```

The algorithms are explained in the main server page [here](https://bitbucket.org/bgunning/tutuka-server/)

